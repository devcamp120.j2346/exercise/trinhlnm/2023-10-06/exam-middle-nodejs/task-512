const contactModel = require("../models/contact.model");
const mongoose = require("mongoose");

const createContact = async (req, res) => {
    // B1: Thu thap du lieu
    const {
        email
    } = req.body;

    // B2: Validate du lieu
    if (!email) {
        return res.status(400).json({
            message: "Yêu cầu email"
        })
    }
    if (validateEmail(email) == false) {
        return res.status(400).json({
            message: "Email không đúng định dạng!"
        })
    }

    try {
        // B3: Xu ly du lieu
        var newContact = {
            email: email
        }

        const result = await contactModel.create(newContact);

        return res.status(201).json({
            message: "Tạo contact thành công",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const getAllContacts = async (req, res) => {
    // B1: Thu thap du lieu
    // B2: Validate du lieu
    // B3: Xu ly du lieu
    try {
        const result = await contactModel.find();

        return res.status(200).json({
            message: "Lấy danh sách contact thành công",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const getContactById = async (req, res) => {
    // B1: Thu thap du lieu
    const contactId = req.params.contactId;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(contactId)) {
        return res.status(400).json({
            message: "Contact ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await contactModel.findById(contactId);

        if (result) {
            return res.status(200).json({
                message: "Lấy thông tin contact thành công",
                data: result
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin contact"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const updateContactById = async (req, res) => {
    // B1: Thu thap du lieu
    const contactId = req.params.contactId;
    const {
        email
    } = req.body;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(contactId)) {
        return res.status(400).json({
            message: "Contact ID không hợp lệ"
        })
    }

    if (!email) {
        return res.status(400).json({
            message: "Yêu cầu email"
        })
    }
    if (validateEmail(email) == false) {
        return res.status(400).json({
            message: "Email không đúng định dạng!"
        })
    }

    // B3: Xu ly du lieu
    try {
        var newUpdateContact = {
            email: email
        };

        const result = await contactModel.findByIdAndUpdate(contactId, newUpdateContact);

        if (result) {
            const finalResult = await contactModel.findById(contactId);
            return res.status(200).json({
                message: "Update thông tin contact thành công",
                data: finalResult
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin contact"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const deleteContactById = async (req, res) => {
    // B1: Thu thap du lieu
    const contactId = req.params.contactId;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(contactId)) {
        return res.status(400).json({
            message: "Contact ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await contactModel.findByIdAndRemove(contactId);

        if (result) {
            return res.status(200).json({
                message: "Xóa thông tin contact thành công"
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin contact"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

//hàm kiểm tra số điện thoại
function validatePhone(paramPhone) {
    // Số điện thoại phải nhập là số, có dấu + hoặc số 0 ở đầu; độ dài từ 10 đến 12 ký tự

    if (isNaN(paramPhone)) {
        return false;
    }

    if (paramPhone.charAt(0) != "0" && paramPhone.charAt(0) != "+") {
        return false;
    }

    if (paramPhone.length < 10 || paramPhone.length > 12) {
        return false;
    }

    return true;
}

// hàm kiểm tra email đúng định dạng ko?
function validateEmail(paramEmail) {
    var vValidRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (paramEmail.match(vValidRegex)) {
        return true;
    } else {
        return false;
    }
}

module.exports = {
    createContact,
    getAllContacts,
    getContactById,
    updateContactById,
    deleteContactById
}
