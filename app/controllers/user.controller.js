const userModel = require("../models/user.model");
const mongoose = require("mongoose");

const createUser = async (req, res) => {
    // B1: Thu thap du lieu
    const {
        fullName,
        phone,
        status
    } = req.body;

    // B2: Validate du lieu
    if (!fullName) {
        return res.status(400).json({
            message: "Yêu cầu full name"
        })
    }

    if (!phone) {
        return res.status(400).json({
            message: "Yêu cầu phone"
        })
    }
    if (validatePhone(phone) == false) {
        return res.status(400).json({
            message: "Phone không đúng định dạng!"
        })
    }

    try {
        // B3: Xu ly du lieu

        var newUser = {
            fullName: fullName,
            phone: phone,
            status: status
        }

        const result = await userModel.create(newUser);

        return res.status(201).json({
            message: "Tạo user thành công",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const getAllUsers = async (req, res) => {
    // B1: Thu thap du lieu
    // B2: Validate du lieu
    // B3: Xu ly du lieu
    try {
        const result = await userModel.find();

        return res.status(200).json({
            message: "Lấy danh sách user thành công",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const getUserById = async (req, res) => {
    // B1: Thu thap du lieu
    const userId = req.params.userId;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            message: "User ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await userModel.findById(userId);

        if (result) {
            return res.status(200).json({
                message: "Lấy thông tin user thành công",
                data: result
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin user"
            })
        }

    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const updateUserById = async (req, res) => {
    // B1: Thu thap du lieu
    const userId = req.params.userId;
    const {
        fullName,
        phone,
        status
    } = req.body;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            message: "User ID không hợp lệ"
        })
    }

    if (phone && validatePhone(phone) == false) {
        return res.status(400).json({
            message: "Phone không đúng định dạng!"
        })
    }

    // B3: Xu ly du lieu
    try {
        var newUpdateUser = {};
        if (fullName) {
            newUpdateUser.fullName = fullName;
        }
        if (phone) {
            newUpdateUser.phone = phone;
        }
        if (status) {
            newUpdateUser.status = status;
        }

        const result = await userModel.findByIdAndUpdate(userId, newUpdateUser);

        if (result) {
            const finalResult = await userModel.findById(userId);
            return res.status(200).json({
                message: "Update thông tin user thành công",
                data: finalResult
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin user"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

const deleteUserById = async (req, res) => {
    // B1: Thu thap du lieu
    const userId = req.params.userId;

    // B2: Validate du lieu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            message: "User ID không hợp lệ"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await userModel.findByIdAndRemove(userId);

        if (result) {
            return res.status(200).json({
                message: "Xóa thông tin user thành công"
            })
        } else {
            return res.status(404).json({
                message: "Không tìm thấy thông tin user"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Đã có lỗi!"
        })
    }
}

//hàm kiểm tra số điện thoại
function validatePhone(paramPhone) {
    // Số điện thoại phải nhập là số, có dấu + hoặc số 0 ở đầu; độ dài từ 10 đến 12 ký tự

    if (isNaN(paramPhone)) {
        return false;
    }

    if (paramPhone.charAt(0) != "0" && paramPhone.charAt(0) != "+") {
        return false;
    }

    if (paramPhone.length < 10 || paramPhone.length > 12) {
        return false;
    }

    return true;
}

module.exports = {
    createUser,
    getAllUsers,
    getUserById,
    updateUserById,
    deleteUserById
}
