const getAllContactsMiddleware = (req, res, next) => {
    console.log("GET all contacts middleware");

    next();
}

const createContactMiddleware = (req, res, next) => {
    console.log("POST contact middleware");

    next();
}

const getContactByIDMiddleware = (req, res, next) => {
    console.log("GET contact by id middleware");

    next();
}

const updateContactMiddleware = (req, res, next) => {
    console.log("PUT contact middleware");

    next();
}

const deleteContactMiddleware = (req, res, next) => {
    console.log("DELETE contact middleware");

    next();
}

module.exports = {
    getAllContactsMiddleware,
    createContactMiddleware,
    getContactByIDMiddleware,
    updateContactMiddleware,
    deleteContactMiddleware
}
