const express = require("express");

const router = express.Router();

const {
    getAllContactsMiddleware,
    createContactMiddleware,
    getContactByIDMiddleware,
    updateContactMiddleware,
    deleteContactMiddleware
} = require("../middlewares/contact.middleware");

const {
    createContact,
    getAllContacts,
    getContactById,
    updateContactById,
    deleteContactById
} = require("../controllers/contact.controller");


router.get("/", getAllContactsMiddleware, getAllContacts);

router.post("/", createContactMiddleware, createContact);

router.get("/:contactId", getContactByIDMiddleware, getContactById)

router.put("/:contactId", updateContactMiddleware, updateContactById)

router.delete("/:contactId", deleteContactMiddleware, deleteContactById)

module.exports = router;

