const express = require("express");

const router = express.Router();

const {
    getAllUsersMiddleware,
    createUserMiddleware,
    getUserByIDMiddleware,
    updateUserMiddleware,
    deleteUserMiddleware
} = require("../middlewares/user.middleware");

const {
    createUser,
    getAllUsers,
    getUserById,
    updateUserById,
    deleteUserById
} = require("../controllers/user.controller");


router.get("/", getAllUsersMiddleware, getAllUsers);

router.post("/", createUserMiddleware, createUser);

router.get("/:userId", getUserByIDMiddleware, getUserById)

router.put("/:userId", updateUserMiddleware, updateUserById)

router.delete("/:userId", deleteUserMiddleware, deleteUserById)

module.exports = router;

