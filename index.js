const express = require("express");
const path = require("path");
const mongoose = require('mongoose');

const app = express();

const port = 8000;

const userRouter = require("./app/routes/user.router");
const contactRouter = require("./app/routes/contact.router");

app.use(express.json());

app.use(express.static(__dirname + "/app/views"));

app.use((req, res, next) => {
    console.log("Thời gian hiện tại:", new Date());

    next();
});

app.use((req, res, next) => {
    console.log("Url của request:", req.url);

    next();
});

// Khai báo kết nối mongoDB qua mongoose
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Vac")
    .then(() => {
        console.log("Connect mongoDB Successfully");
    })
    .catch((err) => {
        console.log(err);
    });

app.get("/", (req, res) => {
    res.sendFile(path.join(__dirname + "/app/views/index.html"));
});

app.get("/admin/users", (req, res) => {
    res.sendFile(path.join(__dirname + "/app/views/user.admin.html"));
});

app.get("/admin/contacts", (req, res) => {
    res.sendFile(path.join(__dirname + "/app/views/contact.admin.html"));
});

app.use("/api/v1/users", userRouter);
app.use("/api/v1/contacts", contactRouter);

app.listen(port, () => {
    console.log(`App Listening on port ${port}`);
});